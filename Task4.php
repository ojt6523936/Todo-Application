<?php 
class API {
    function studName($name){
        return "Full Name: $name"; 
    }

    function studHobby(array $hobby){
        echo "Hobbies: ";
        echo "<ul>";
        foreach ($hobby as $hobbies){
            echo $hobbies."<br>";
        }
        echo "</ul>";
    }
    
    function studInfo($info){
        echo "Age: ".$info->age."<br>";
        echo "Email: ".$info->email."<br>";
        echo "Birthday: ".$info->bday."<br>";

    }
}
$api = new API;
//1st function
echo $api->studName("Michael Manzon");
echo "<br>";
//2nd function
$arrHobby = ["Playing Online Games", "Watching Anime", "Reading Manga"];
echo $api->studHobby($arrHobby);
//3rd function
$info = (object) [
    'age' => 23,
    'email' => 'mike.manzon16@gmail.com',
    'bday' => 'May 16,2000',
];
echo $api->studInfo($info);

?>