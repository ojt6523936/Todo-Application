<?php
require_once 'API.php';
use PHPUnit\Framework\TestCase;

class APITest extends TestCase
{
    protected function setUp(): void
    {
        $this->api = new API();
    }

    private $newlyInsertedId; 

    public function testHttpPost()
    {
        $_SERVER['REQUEST_METHOD'] = 'POST';

        $payload = array(
            'first_name' => 'Test',
            'middle_name' => 'test',
            'last_name' => 'last test',
            'contact_number' => 654655
        );  
        $result = json_decode($this->api->httpPost($payload), true);
        $this->assertArrayHasKey('status', $result);        
        $this->assertEquals($result['status'], 'success');
        $this->assertArrayHasKey('data', $result);  

        $this->newlyInsertedId = $result['data'];
        return $this->newlyInsertedId;
    }
    public function testHttpGet()
    {
        $_SERVER['REQUEST_METHOD'] = 'GET';

        $payload = array(
            
        );
        $result = json_decode($this->api->httpGet($payload), true);
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'success');
        $this->assertArrayHasKey('data', $result);
    }

    /**
     * @depends testHttpPost
     */ 
    public function testHttpPut($id)
    {
        $_SERVER['REQUEST_METHOD'] = 'PUT';
        $payload = array(
            'id' => $id,
            'first_name' => 'testFName',
            'middle_name' => 'testMName',   
            'last_name' => 'testLName',
            'contact_number' => 123456789
        );
        $result = json_decode($this->api->httpPut($id, $payload), true);
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'success');
        $this->assertArrayHasKey('data', $result);
    }

    /**
     * @depends testHttpPost
     */
    public function testHttpDelete($id)
    {
        $_SERVER['REQUEST_METHOD'] = 'DELETE';
        $payload = [
            'id' => $id
        ];

        $result = json_decode($this->api->httpDelete($id, $payload), true);
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'success');
        $this->assertArrayHasKey('data', $result);
    }

    
}


  

