<?php
 /**
   * Tells the browser to allow code from any origin to access
   */
  header("Access-Control-Allow-Origin: *");

  /**
   * Tells browsers whether to expose the response to the frontend JavaScript code
   * when the request's credentials mode (Request.credentials) is include
   */
  header("Access-Control-Allow-Credentials: true");
 


  /**
   * Specifies one or more methods allowed when accessing a resource in response to a preflight request
   */
  header("Access-Control-Allow-Methods: POST, GET, PUT, DELETE");
 
  /**
   * Used in response to a preflight request which includes the Access-Control-Request-Headers to
   * indicate which HTTP headers can be used during the actual request
   */
  header("Access-Control-Allow-Headers: Content-Type");

  require_once('MysqliDb.php');
  
  
  class API {
    public function __construct(){
        $this->db = new MysqliDb('localhost', 'root', '', 'employee');
    }

     /**
       * HTTP GET Request
       *
       * @param $payload
       */

    public function httpGet ($payload){

        if(!is_array($payload)){
            return json_encode('Payload must be an array');
        }  
        $GetResult = $this->db->get('information');
        if($GetResult) {
            return $this->successResponse('Data retrieved successfully',$GetResult);
        }
        else
           {
             return $this->errorResponse('GET','Failed fetch request');
           }
        
    }

    /**
       * HTTP POST Request
       *
       * @param $payload
       */

    public function httpPost($payload)
      {
        if(!is_array($payload) && empty($payload)){
            return json_encode('Payload must be a non-empty array');
        } 

        $PostResult = $this->db->insert('information',$payload);
        if($PostResult) {
            return $this->successResponse('Data retrieved successfully',$PostResult);
        }
        else
           {
             return $this->errorResponse('POST','Failed fetch request');
           }
      }

       /**
       * HTTP PUT Request
       *
       * @param $id
       * @param $payload
       */

    public function httpPut($id, $payload)
        {
            if (empty($id)) {
                return $this->errorResponse('PUT', 'ID must not be empty');
            }
        
            if (empty($payload)) {
                return $this->errorResponse('PUT', 'Payload must not be empty');
            }

        if($payload['id'] !== $id){
            return $this->errorResponse('PUT','ID mismatch between parameter and payload');
        }
        $this->db->where('id',$id);
        $PutResult = $this->db->update('information',$payload);

        if($PutResult) {
            return $this->successResponse('Data updated successfully',$PutResult);
        }
        else {
            return $this->successResponse('PUT','Failed to Update Data');                      
        }
        }


         /**
       * HTTP DELETE Request
       *
       * @param $id
       * @param $payload
       */

    public function httpDelete($id,$payload)
        {
            if (empty($id)) {
                return $this->errorResponse('DELETE', 'ID must not be empty');
            }
            if (empty($payload)) {
                return $this->errorResponse('DELETE', 'Payload must not be empty');
            }
            if($payload['id'] !== $id){
            return $this->errorResponse('DELETE','ID mismatch between parameter and payload');
            }
            if (!is_array($payload)) {
                $this->db->where('id', $id);
            } else {
                $this->db->where('id', $payload,'IN');
            }

            $deleteResult = $this->db->delete('information');
            
        
            if ($deleteResult) {
                return $this->successResponse('Data deleted successfully');
            } else {
                return $this->errorResponse('DELETE', 'Failed to Delete Data');
            }
    }
    

    /**
     * ERROR RESPONSE
     */
    private function errorResponse($method,$message){
        return json_encode(array(
            'method' => $method,
            'status' => 'failed',
            'message' => $message
        ));
    }
    /**
     * SUCCESS RESPONSE
     */
    private function successResponse($message,$data=null){
        return json_encode(array('status' => 'success',
        'message' => $message,
        'data' => $data));
    }
}
  
 
  
  $api = new API();


 $request_method = $_SERVER['REQUEST_METHOD'];
  if ($request_method === 'GET') {
    $received_data = $_GET;
}
else {
    //check if method is PUT or DELETE, and get the ids on URL
    if ($request_method === 'PUT' || $request_method === 'DELETE') {
        $request_uri = $_SERVER['REQUEST_URI'];


        $ids = null;
        $exploded_request_uri = array_values(explode("/", $request_uri));


        $last_index = count($exploded_request_uri) - 1;


        $ids = $exploded_request_uri[$last_index];

        }
        //payload data
    $received_data = json_decode(file_get_contents('php://input'), true);
}

 //Checking if what type of request and designating to specific functions
  switch ($request_method) {
    case 'GET':
            echo $api->httpGet($received_data);
    break;

    case 'POST':
            echo $api->httpPost($received_data);
    break;

    case 'PUT':
            echo $api->httpPut($ids, $received_data);
    break;
    case 'DELETE':
            echo $api->httpDelete($ids, $received_data);
    break;
      }

  ?>