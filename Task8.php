
<html>
<head>
    <title>Display Form Data</title>
</head>
<style>
    label {
        width: 100px;
        display: inline-block;
    }
    table{
        border: 1px solid;  
        table-layout: fixed;
    }

    td, th {
        border: 1px solid;
        width: 200px;
        overflow: hidden;
    }
</style>
<body>
<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
    <label for="name">Name:</label>
    <input type="text" name="name" id="name" required><br>

    <label for="age">Age:</label>
    <input type="number" name="age" id="age"><br>

    <label for="address">Address:</label>
    <input type="text" name="address" id="address"><br>

    <input type="submit" name="create" value="Submit">
</form>

<?php
$host = "localhost";
$username = "root";
$password = "";
$database = "info_database";

require_once('MysqliDb.php');

//database connection
$conn = new MysqliDb($host, $username, $password, $database);

class CRUD {
    private $conn;

    public function __construct($connection) {
        $this->conn = $connection;
    }

    public function create() {
        $name = $_POST['name'];
        $age = $_POST['age'];
        $address = $_POST['address'];

        $info = Array (
            "name" => $name,
            "age" => $age,
            "address" => $address
        );
       $id = $this->conn->insert ('info',$info);
       if($id){
        echo 'Data added successfully';
       }
       else {
        echo 'Error:'.$this->conn->getLastError();
       }
    }

    public function read() {
        $info = $this->conn->get('info');
            return $info;
    }

    public function update($id, $name, $age, $address) {
        $info = [
            "name" => $name,
            "age" => $age,
            "address" => $address
        ];
        $this->conn->where ('id',$id);

        if ($this->conn->update('info',$info)){
            echo 'Data updated successfully';
        }
        else {
            echo 'Error:'.$this->conn->getLastError();
        }
    }

    public function delete($id) {
       $this->conn->where('id',$id);

       if($this->conn->delete('info')){
        echo "Data deleted successfully!";
        } else {
            echo "Error: " . $this->conn->getLastError();
        }
       
    }
}

$crud = new CRUD($conn);

//For Creating Records
if (isset($_POST['create'])) {
    $crud->create();
}

//For Updating Records
if (isset($_POST['update'])) {
    $update_id = $_POST['update_id'];
    $update_name = $_POST['update_name'];
    $update_age = $_POST['update_age'];
    $update_address = $_POST['update_address'];
    $crud->update($update_id, $update_name, $update_age, $update_address);
}
//For Deleting Records
if (isset($_POST['delete'])) {
    $delete_id = $_POST['delete_id'];
    $crud->delete($delete_id);
}

//For Displaying the Records
$result = $crud->read();
echo "<br>";

if ($result) {
    echo "<table>";
    echo "<thead>
                <tr>
                    <th>Name</th>
                    <th>Age</th>
                    <th>Address</th>
                    <th colspan='2'>Operation</th>
                </tr>
            </thead>";
    echo "<tbody>";
    foreach ($result as $row) {
        echo "
            <tr>
                <td>" . $row['name'] . "</td>
                <td>" . $row['age'] . "</td>
                <td>" . $row['address'] . "</td>
                <td>
                    <form method='post' action='" . $_SERVER['PHP_SELF'] . "'>
                        <input type='hidden' name='update_id' value='" . $row['id'] . "'>
                        <input type='text' name='update_name' placeholder='New Name'>
                        <input type='number' name='update_age' placeholder='New Age'>
                        <input type='text' name='update_address' placeholder='New Address'>
                        <input type='submit' name='update' value='Update'>
                    </form>
                </td>
                <td>
                    <form method='post' action='" . $_SERVER['PHP_SELF'] . "'>
                        <input type='hidden' name='delete_id' value='" . $row['id'] . "'>
                        <input type='submit' name='delete' value='Delete'>
                    </form>
                </td>
            </tr>
        ";
    }
    echo "</tbody>";
    echo "</table>";
} else {
    echo "No results";
}

?>
</body>
</html>
