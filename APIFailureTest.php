<?php


require_once 'API.php';
use PHPUnit\Framework\TestCase;



class APIFailTest extends TestCase
{
    protected function setUp(): void
    {
        $this->api = new API();
    }

    public function testFailedHttpPost()
    {
        $_SERVER['REQUEST_METHOD'] = 'POST';

        $payload = 'invalid_payload';

        $result = json_decode($this->api->httpPost($payload), true);
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'failed');
    }

    public function testFailedHttpGet()
{
    $_SERVER['REQUEST_METHOD'] = 'GET';
    $payload = array();
    $result = json_decode($this->api->httpGet($payload), true);
    $this->assertIsArray($result); 
    $this->assertArrayHasKey('status', $result);
    $this->assertEquals('failed', $result['status']);
}

public function testFailedHttpPut()
{
    $_SERVER['REQUEST_METHOD'] = 'PUT';
    $id = '';
    $payload = array(
        'id' => $id,
    );

    $result = json_decode($this->api->httpPut($id, $payload), true);
    $this->assertIsArray($result);
    $this->assertArrayHasKey('status', $result);
    $this->assertEquals('failed', $result['status']);
    
}

public function testFailedHttpDelete()
{
    $_SERVER['REQUEST_METHOD'] = 'DELETE';
    $id = '';
    $payload = array(
        'id' => $id,
    );

    $result = json_decode($this->api->httpDelete($id, $payload), true);
    $this->assertIsArray($result);
    $this->assertArrayHasKey('status', $result);
    $this->assertEquals('failed', $result['status']);
    
}

}
?>
